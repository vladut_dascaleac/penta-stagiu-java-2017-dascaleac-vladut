package Controler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConvertor {
    DateFormat df = new SimpleDateFormat ("yyyy-MM-dd"); 
    private Date date;
    private String sDate;
    public DateConvertor(){}
    
    public Date stringToDate(String sDate)
    {
        try{
            date = df.parse(sDate); 
        }catch(Exception e){
            System.err.println(e);
        }
        return date;
    }
    public String dateToString(Date date){
         try{
            sDate = df.format(date);
        }catch(Exception e){
            System.err.println(e);
        }
        return sDate;
    }
    
}
