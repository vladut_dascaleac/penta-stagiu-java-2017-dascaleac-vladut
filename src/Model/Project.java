package Model;

import java.util.Date;

public class Project {
    private String idProject;
    private String idEmployee;
    private String projectName;
    private String description;
    private Date startProject;
    private Date endProject;

    public Project(String idProject, String idEmployee, String projectName, String description, Date startProject, Date endProject) {
        this.idProject = idProject;
        this.idEmployee = idEmployee;
        this.projectName = projectName;
        this.description = description;
        this.startProject = startProject;
        this.endProject = endProject;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(String idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartProject() {
        return startProject;
    }

    public void setStartProject(Date startProject) {
        this.startProject = startProject;
    }

    public Date getEndProject() {
        return endProject;
    }

    public void setEndProject(Date endProject) {
        this.endProject = endProject;
    }
    
    /*
    @Override
    public boolean equals(Object obj){
        Project project;
        if(obj == null)
            return false;
        if(obj instanceof Project){
            project = (Project) obj;
            if(this.idProject.equals(project.getIdProject()))
                return true;
        }
        return false;
    }
    */
}
