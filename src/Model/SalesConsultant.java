package Model;

import java.util.Date;

public class SalesConsultant extends Employee{
    private float commision;
    private float sales;
    
    public SalesConsultant(String id, String name, String gender, String idDepartment, String positon, Date startWorking, Date endWorking, float commision, float sales) {
        super(id, name, gender, idDepartment, positon, startWorking, endWorking);
        this.commision = commision;
        this.sales = sales;
    }

    public float getCommision() {
        return commision;
    }

    public void setCommisionRate(float commision) {
        this.commision = commision;
    }

    public float getSales() {
        return sales;
    }

    public void setSales(float sales) {
        this.sales = sales;
    }
    
}
