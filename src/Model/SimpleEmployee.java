package Model;

import java.util.Date;

public class SimpleEmployee extends Employee{
    public SimpleEmployee(String id, String name, String gender, String idDepartment, String positon, Date startWorking, Date endWorking) {
        super(id, name, gender, idDepartment, positon, startWorking, endWorking);
    }
}
