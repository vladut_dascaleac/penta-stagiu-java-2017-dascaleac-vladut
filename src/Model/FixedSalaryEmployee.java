package Model;

import java.util.Date;

public class FixedSalaryEmployee extends Employee{
    
    private float salary;
    private float experience;

    public FixedSalaryEmployee(String id, String name, String gender, String idDepartment, String position, Date startWorking, Date endWorking,float salary,float experience) {
        super(id, name, gender, idDepartment, position, startWorking, endWorking);
        this.salary = salary;
        this.experience = experience;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public float getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }
}
