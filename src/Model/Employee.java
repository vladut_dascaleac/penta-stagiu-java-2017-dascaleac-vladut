package Model;

import java.util.Date;

public abstract class Employee {
    private String id;
    private String name;
    private String gender;
    private String idDepartment;
    private String position;
    private Date startWorking;
    private Date endWorking;
    private Boolean isDelete;

    public Employee(String id, String name, String gender, String idDepartment, String positon, Date startWorking, Date endWorking) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.idDepartment = idDepartment;
        this.position = positon;
        this.startWorking = startWorking;
        this.endWorking = endWorking;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(String idDepartment) {
        this.idDepartment = idDepartment;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Employee(String id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public Date getStartWorking() {
        return startWorking;
    }

    public void setStartWorking(Date startWorking) {
        this.startWorking = startWorking;
    }

    public Date getEndWorking() {
        return endWorking;
    }

    public void setEndWorking(Date endWorking) {
        this.endWorking = endWorking;
    }

    public int getIsDelete() {
        if(isDelete)
            return 1;
        return 0;
    }

    public void setIsDeleted(Boolean isDelete) {
        this.isDelete = isDelete;
    }

}
