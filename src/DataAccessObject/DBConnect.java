package DataAccessObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnect {
    private final String host;
    private final String DBUser;
    private final String DBPassword ;
    private Connection con;
    private Statement stm;
    
    public DBConnect(){
        this.host = "jdbc:mysql://fenrir.info.uaic.ro/itst";
        this.DBUser = "itst";
        this.DBPassword = "KOvkUYjHDD";
    }
    public Connection getConnection()
    {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection( host, DBUser, DBPassword);
        }catch(SQLException e){
            System.err.println(e);
        }catch(ClassNotFoundException e) {
            System.err.println("Class not found "+ e);
        }catch(Exception e){
            System.err.println(e);
        }
        return this.con;
    }
    public Statement getStatement() throws SQLException{
        this.stm = getConnection().createStatement();
        return this.stm;
    }
}
