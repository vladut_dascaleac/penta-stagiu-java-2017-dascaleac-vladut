package DataAccessObject;

import Model.SalesConsultant;
import java.util.List;

public interface SalesConsultantDAO {
    public void addSalesConsultant(SalesConsultant consultant);
    public List<SalesConsultant> getAllSalesConsultant();
    public SalesConsultant getSalesConsultant(String id);
    public void updateSalesConsultant(SalesConsultant consultant);
    public void deleteSalesConsultant(SalesConsultant consultant);
}
