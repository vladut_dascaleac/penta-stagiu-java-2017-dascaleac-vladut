package DataAccessObject;

import Model.Employee;
import java.util.List;

public interface EmployeeDAO {
    
    public List<Employee> getAllEmployee();
    public void addEmployee(Employee emp);
    public Employee getEmployee(String id);
    public void updateEmployee(Employee emp);
    public void deleteEmployee(Employee emp);
    
}
