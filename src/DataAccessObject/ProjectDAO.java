package DataAccessObject;

import Model.Project;
import java.util.List;

public interface ProjectDAO {
    public void addProject(Project project);
    public List<Project> getAllProjects();
    public List<Project> getProject(String id);
    public void updateProject(Project project);
    public void deleteProject(Project project);
    public void finishProject(Project project);
}
