package DataAccessObject;
import Controler.DateConvertor;
import Model.FixedSalaryEmployee;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FixedSalaryEmployeeDAOImpl implements FixedSalaryEmployeeDAO {

    private Statement stm;
    private final DBConnect dbConnect;
    private final Connection conn;
    private String sql;
    private ResultSet rs;
    private final DateConvertor dc;
    private List<FixedSalaryEmployee> employees;
    private final EmployeeDAO empDAO;
    
    public FixedSalaryEmployeeDAOImpl(){
        dbConnect = new DBConnect();
        conn = dbConnect.getConnection();
        employees = new ArrayList<>();
        dc = new DateConvertor();
        empDAO = new EmployeeDAOImpl();
    }
    @Override
    public void addFSEmployee(FixedSalaryEmployee emp) {
        /* Prima data adaugam angajatul in tabela Employee si dupa in tabela FixedSalaryEmployee */
        empDAO.addEmployee(emp);
            try{
            stm = dbConnect.getStatement();
            sql = "INSERT INTO FixedSalaryEmployee (id, salary, experience) VALUES ('"
                    +emp.getId()+"','"
                    +emp.getSalary()+"','"
                    +emp.getExperience()+"')";
            stm.executeUpdate(sql);
        }catch(SQLException e){
            System.err.println(e);
        }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }
    }

    @Override
    public List<FixedSalaryEmployee> getAllFSEmployee() {
            try {
            employees = new ArrayList<>();
            stm = dbConnect.getStatement();
            sql = "SELECT Employee.id, Employee.name, Employee.gender, Employee.idDepartment, Employee.position, Employee.startWorking, Employee.endWorking, FixedSalaryEmployee.salary, FixedSalaryEmployee.experience " 
                    +"FROM FixedSalaryEmployee "
                    +"JOIN Employee ON FixedSalaryEmployee.id = Employee.id "
                    +"AND Employee.isDelete =0";
            rs = stm.executeQuery(sql);
            while(rs.next()){
                employees.add(
                        new FixedSalaryEmployee(
                                rs.getString("id"),
                                rs.getString("name"),
                                rs.getString("gender"),
                                rs.getString("idDepartment"),
                                rs.getString("position"),
                                rs.getDate("startWorking"),
                                rs.getDate("endWorking"),
                                rs.getFloat("salary"),
                                rs.getFloat("experience")
                            )
                            );
            }
            rs.close();
        }catch(SQLException e){
            System.err.println(e);
        }catch(Exception e){
            System.err.println(e);
        }finally{
        //finally block used to close resources
        try{
            if(stm != null)
                conn.close();
        }catch(Exception e){
            System.err.println("Nu s-a putut inchide conexiunea "+e);
        }
        }
        return employees;
    }

    @Override
    public FixedSalaryEmployee getFSEmployee(String id) {
        FixedSalaryEmployee fsEmp = null;
        try {
            stm = dbConnect.getStatement();
            sql = "SELECT Employee.id, Employee.name, Employee.gender, Employee.idDepartment, Employee.position, Employee.startWorking, Employee.endWorking, FixedSalaryEmployee.salary, FixedSalaryEmployee.experience " 
                    +"FROM FixedSalaryEmployee "
                    +"JOIN Employee ON FixedSalaryEmployee.id = '"
                    + id +"' AND Employee.id ='"+id
                    +"' AND Employee.isDelete = 0";
            rs = stm.executeQuery(sql);
            if(rs.next()){
                fsEmp = new FixedSalaryEmployee(
                                rs.getString("id"),
                                rs.getString("name"),
                                rs.getString("gender"),
                                rs.getString("idDepartment"),
                                rs.getString("position"),
                                rs.getDate("startWorking"),
                                rs.getDate("endWorking"),
                                rs.getFloat("salary"),
                                rs.getFloat("experience")
                            );
            }
            rs.close();
        }catch(SQLException e){
            System.err.println(e);
        }catch(Exception e){
            System.err.println(e);
        }finally{
        //finally block used to close resources
        try{
            if(stm != null)
                conn.close();
        }catch(Exception e){
            System.err.println("Nu s-a putut inchide conexiunea "+e);
        }
        }
     return fsEmp;               
    }

    @Override
    public void updateFSEmployee(FixedSalaryEmployee updateEmp) {
        /* Prima data adaugam angajatul in tabela Employee si dupa in tabela FixedSalaryEmployee */
        /*Angajatul trebuie sa nu fie sters din baza de date pentru a i se putea face update*/
        if(getFSEmployee(updateEmp.getId()) != null)
        {
            empDAO.updateEmployee(updateEmp);
                try {
                    stm = dbConnect.getStatement();
                    sql = "UPDATE FixedSalaryEmployee SET "
                           + "salary = '" + updateEmp.getSalary()
                           +"', experience = '"+updateEmp.getExperience() 
                           +"' WHERE id = '"+updateEmp.getId()+"'"; 
                    stm.executeUpdate(sql);
                } catch (SQLException e) {
                    System.err.println("\n---> FSEmployee "+e);
                }finally{
                 try{
                    if(stm!=null)
                        conn.close();
                }catch(SQLException e){
                    System.err.println(e);
                }
                }
        }
        else
            System.out.println("Angajatul este sters din baza de date!");
    }

    @Override
    public void deleteFSEmployee(FixedSalaryEmployee emp) {
       empDAO.deleteEmployee(emp);
    }

}
