package DataAccessObject;

import Model.Employee;
import Model.SimpleEmployee;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import Controler.DateConvertor;

public class EmployeeDAOImpl implements EmployeeDAO {
    
    private Statement stm;
    private final DBConnect dbConnect;
    private final Connection conn;
    private String sql;
    private ResultSet rs;
    private final DateConvertor dc;
    private final List<Employee> employees;
    
    public EmployeeDAOImpl(){
        dbConnect = new DBConnect();
        conn = dbConnect.getConnection();
        employees = new ArrayList<>();
        dc = new DateConvertor();
    }

    @Override
    public List<Employee> getAllEmployee(){
        try {
            stm = dbConnect.getStatement();
            sql = "SELECT * FROM Employee";
            rs = stm.executeQuery(sql);
            while(rs.next()){
                employees.add(
                        new SimpleEmployee(
                                rs.getString("id"),
                                rs.getString("name"),
                                rs.getString("gender"),
                                rs.getString("idDepartment"),
                                rs.getString("position"),
                                rs.getDate("startWorking"),
                                rs.getDate("endWorking")
                            )
                            );
            }
            rs.close();
        }catch(SQLException e){
            System.err.println(e);
        }catch(Exception e){
            System.err.println(e);
        }finally{
        //finally block used to close resources
        try{
            if(stm != null)
                conn.close();
        }catch(Exception e){
            System.err.println("Nu s-a putut inchide conexiunea "+e);
        }
        }
        return employees;
    }

    @Override
    public void addEmployee(Employee emp) {
        try{
            stm = dbConnect.getStatement();
            if(emp.getStartWorking() != null && emp.getEndWorking() != null)
            {
            sql = "INSERT INTO Employee (id,name,gender,idDepartment,position,startWorking,endWorking,isDelete) VALUES ('"
                    +emp.getId()+"','"
                    +emp.getName()+"','"
                    +emp.getGender()+"','"
                    +emp.getIdDepartment()+"','"
                    +emp.getPosition()+"',"
                    +"(STR_TO_DATE('"+dc.dateToString(emp.getStartWorking())+"','%Y-%m-%d')),"
                    +"(STR_TO_DATE('"+dc.dateToString(emp.getEndWorking())+"','%Y-%m-%d')),"
                    +false+")";
            }
            /*Diferenta de ghilimele intre o valoarea string valida si una NULL*/
            if(emp.getStartWorking() != null && emp.getEndWorking() == null)
            {
            sql = "INSERT INTO Employee (id,name,gender,idDepartment,position,startWorking,endWorking,isDelete) VALUES ('"
                    +emp.getId()+"','"
                    +emp.getName()+"','"
                    +emp.getGender()+"','"
                    +emp.getIdDepartment()+"','"
                    +emp.getPosition()+"',"
                    +"(STR_TO_DATE('"+dc.dateToString(emp.getStartWorking())+"','%Y-%m-%d')),"
                    +null+","
                    +false+")";
            }
            stm.executeUpdate(sql);
        }catch(SQLException e){
            System.err.println(e);
        }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }
    }
    
    @Override
    public Employee getEmployee(String id) {
        Employee emp = null;
        try{
            stm = dbConnect.getStatement();
            sql = "SELECT * FROM Employee"
                + "WHERE id = " + id 
                + "AND isDelete = 0";
            rs = stm.executeQuery(sql);
            rs.next();
                emp = new SimpleEmployee
                                (rs.getString("id"),
                                rs.getString("name"),
                                rs.getString("gender"),
                                rs.getString("idDepartment"),
                                rs.getString("position"),
                                rs.getDate("startWorking"),
                                rs.getDate("endWorking")
                                );
            
        }catch(SQLException e){
            System.err.println(e);
        }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }
        return emp;
    }

    @Override
    public void updateEmployee(Employee updateEmployee) {
        /*
        if(updateEmployee.getEndWorking() == null) {
            try {
                stm = dbConnect.getStatement();
                sql = "UPDATE Employee SET "
                    + "name = '" + updateEmployee.getName()
                    + "', gender = '" +updateEmployee.getGender()
                    +"', idDepartment = '"+updateEmployee.getIdDepartment()
                    +"', position = '"+updateEmployee.getPosition()
                    +"', startWorking = '(STR_TO_DATE('"+dc.dateToString(updateEmployee.getStartWorking())+"','%Y-%m-%d'))'"
                    +", endWorking = " + null
                    +" WHERE id = '" + updateEmployee.getId()+"'";
                stm.executeUpdate(sql);
            } catch (SQLException e) {
                System.err.println("\n---> EmployeeDAO "+e);
            }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println(e);
            }
            }
        }
        */
        //if(updateEmployee.getEndWorking() != null) {
            try {
                stm = dbConnect.getStatement();
                sql = "UPDATE Employee SET "
                    + "name = '" + updateEmployee.getName()
                    + "', gender = '" +updateEmployee.getGender()
                    +"', idDepartment = '"+updateEmployee.getIdDepartment()
                    +"', position = '"+updateEmployee.getPosition()
                    +"', startWorking = (STR_TO_DATE('"+dc.dateToString(updateEmployee.getStartWorking())+"','%Y-%m-%d'))"
                    +", endWorking = (STR_TO_DATE('"+dc.dateToString(updateEmployee.getEndWorking())+"','%Y-%m-%d'))"
                    +" WHERE id = '"+updateEmployee.getId()+"'";
                stm.executeUpdate(sql);
            } catch (SQLException e) {
                System.err.println(e);
            }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println(e);
            }
            }
        //}
    }

    @Override
    public void deleteEmployee(Employee emp) {
        try{
        stm = dbConnect.getStatement();
        sql = "UPDATE Employee SET isDelete = 1 "
            + "WHERE id = '"+emp.getId()+"'";
        stm.executeUpdate(sql);
        } catch (SQLException e) {
            System.err.println("Clasa EmployeeDAOImpl, metoda deleteEmployee "+e);
        }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println("Clasa EmployeeDAOImpl, metoda deleteEmployee "+e);
            }
        }
        /*
        try{
        String sql = "DELETE FROM Employee WHERE "
                    +"id = "+emp.getId();
        stm.executeUpdate(sql);
        }catch(SQLException e){
            System.err.println(e);
        }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }
        */
    }
}
