package DataAccessObject;

import Model.FixedSalaryEmployee;
import java.util.List;

public interface FixedSalaryEmployeeDAO {
    public void addFSEmployee(FixedSalaryEmployee emp);
    public List<FixedSalaryEmployee> getAllFSEmployee();
    public FixedSalaryEmployee getFSEmployee(String id);
    public void updateFSEmployee(FixedSalaryEmployee emp);
    public void deleteFSEmployee(FixedSalaryEmployee emp);
}
