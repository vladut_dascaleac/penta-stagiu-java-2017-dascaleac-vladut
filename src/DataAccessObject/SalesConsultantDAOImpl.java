package DataAccessObject;

import Controler.DateConvertor;
import Model.SalesConsultant;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SalesConsultantDAOImpl implements SalesConsultantDAO{

    private Statement stm;
    private final DBConnect dbConnect;
    private final Connection conn;
    private String sql;
    private ResultSet rs;
    private final DateConvertor dc;
    private List<SalesConsultant> empSales;
    private EmployeeDAO empDAO;
    
    public SalesConsultantDAOImpl(){
        dbConnect = new DBConnect();
        conn = dbConnect.getConnection();
        empSales = new ArrayList<>();
        dc = new DateConvertor();
        empDAO = new EmployeeDAOImpl();
    }
    
    @Override
    public void addSalesConsultant(SalesConsultant consultant) {
        /* Prima data adaugam angajatul in tabela Employee si dupa in tabela SalesConsultant */
        empDAO.addEmployee(consultant);
            try{
            stm = dbConnect.getStatement();
            sql = "INSERT INTO SalesConsultant (id, commision, sales) VALUES ('"
                    +consultant.getId()+"','"
                    +consultant.getCommision()+"','"
                    +consultant.getSales()+"')";
            stm.executeUpdate(sql);
        }catch(SQLException e){
            System.err.println(e);
        }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }
    }
    
    @Override
    public List<SalesConsultant> getAllSalesConsultant() {
        empSales = new ArrayList<>();
        try {
            stm = dbConnect.getStatement();
            sql = "SELECT Employee.id, Employee.name, Employee.gender, Employee.idDepartment, Employee.position, Employee.startWorking, Employee.endWorking, SalesConsultant.commision, SalesConsultant.sales " 
                    +"FROM SalesConsultant "
                    +"JOIN Employee ON SalesConsultant.id = Employee.id "
                    +"AND Employee.isDelete =0";
            rs = stm.executeQuery(sql);
            while(rs.next()){
                empSales.add(
                        new SalesConsultant(
                                rs.getString("id"),
                                rs.getString("name"),
                                rs.getString("gender"),
                                rs.getString("idDepartment"),
                                rs.getString("position"),
                                rs.getDate("startWorking"),
                                rs.getDate("endWorking"),
                                rs.getFloat("commision"),
                                rs.getFloat("sales")
                            )
                            );
            }
            rs.close();
        }catch(SQLException e){
            System.err.println(e);
        }catch(Exception e){
            System.err.println(e);
        }finally{
        //finally block used to close resources
        try{
            if(stm != null)
                conn.close();
        }catch(Exception e){
            System.err.println("Nu s-a putut inchide conexiunea "+e);
        }
        }
        return empSales;
    }

    @Override
    public SalesConsultant getSalesConsultant(String id) {
        SalesConsultant saleEmp = null;
        try {
            stm = dbConnect.getStatement();
            sql = "SELECT Employee.id, Employee.name, Employee.gender, Employee.idDepartment, Employee.position, Employee.startWorking, Employee.endWorking, SalesConsultant.commision, SalesConsultant.sales " 
                    +"FROM SalesConsultant "
                    +"JOIN Employee ON SalesConsultant.id = '"
                    + id +"' AND Employee.id ='"+id
                    +"' AND Employee.isDelete = 0";
            rs = stm.executeQuery(sql);
            if(rs.next()){
                saleEmp = new SalesConsultant(
                                rs.getString("id"),
                                rs.getString("name"),
                                rs.getString("gender"),
                                rs.getString("idDepartment"),
                                rs.getString("position"),
                                rs.getDate("startWorking"),
                                rs.getDate("endWorking"),
                                rs.getFloat("commision"),
                                rs.getFloat("sales")
                            );
            }
            rs.close();
        }catch(SQLException e){
            System.err.println(e);
        }catch(Exception e){
            System.err.println(e);
        }finally{
        //finally block used to close resources
        try{
            if(stm != null)
                conn.close();
        }catch(Exception e){
            System.err.println("Nu s-a putut inchide conexiunea "+e);
        }
        }
     return saleEmp; 
    }

    @Override
    public void updateSalesConsultant(SalesConsultant updateConsultant) {
                /* Prima data adaugam angajatul in tabela Employee si dupa in tabela FixedSalaryEmployee */
        /*Angajatul trebuie sa nu fie sters din baza de date pentru a i se putea face update*/
        if(getSalesConsultant(updateConsultant.getId()) != null)
        {
                try {
                    empDAO.updateEmployee(updateConsultant);
                    stm = dbConnect.getStatement();
                    sql = "UPDATE SalesConsultant SET "
                           + "commision = '" + updateConsultant.getCommision()
                           +"', sales = '"+updateConsultant.getSales() 
                           +"' WHERE id = '"+updateConsultant.getId()+"'"; 
                    stm.executeUpdate(sql);
                } catch (SQLException e) {
                    System.err.println("\n---> SalesConsultantDAOImpl "+e);
                }finally{
                 try{
                    if(stm!=null)
                        conn.close();
                }catch(SQLException e){
                    System.err.println(e);
                }
                }
        }
        else
            System.out.println("Angajatul este sters din baza de date!");
    
    }

    @Override
    public void deleteSalesConsultant(SalesConsultant consultant) {
        empDAO.deleteEmployee(consultant);
    }
}
