package DataAccessObject;

import Controler.DateConvertor;
import Model.Project;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProjectDAOImpl implements ProjectDAO{
    private Statement stm;
    private final DBConnect dbConnect;
    private final Connection conn;
    private String sql;
    private ResultSet rs;
    private final DateConvertor dc;
    private List<Project> projects;
    
    public ProjectDAOImpl(){
        projects = new ArrayList<>();
        dbConnect = new DBConnect();
        conn = dbConnect.getConnection();
        dc = new DateConvertor();
    }
    @Override
    public void addProject(Project project){
        projects = new ArrayList<>();
            try{
            stm = dbConnect.getStatement();
            sql = "INSERT INTO Project (idProject, idEmployee, projectName, description, startProject, endProject, isDelete) VALUES ('"
                    +project.getIdProject()+"','"
                    +project.getIdEmployee()+"','"
                    +project.getProjectName()+"','"
                    +project.getDescription()+"',"
                    +"(STR_TO_DATE('"+dc.dateToString(project.getStartProject())+"','%Y-%m-%d')),"
                    +"(STR_TO_DATE('"+dc.dateToString(project.getEndProject())+"','%Y-%m-%d')),"
                    +false+")";
            stm.executeUpdate(sql);
        }catch(SQLException e){
            System.err.println(e);
        }finally{
             try{
                if(stm!=null)
                    conn.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }   
    }
    @Override
    public List<Project> getAllProjects() {
        projects = new ArrayList<>();
        try {
            stm = dbConnect.getStatement();
            sql = "SELECT * FROM Project WHERE isDelete = 0";
            rs = stm.executeQuery(sql);
            while(rs.next()){
                projects.add(
                        new Project(
                                rs.getString("idProject"),
                                rs.getString("idEmployee"),
                                rs.getString("projectName"),
                                rs.getString("description"),
                                rs.getDate("startProject"),
                                rs.getDate("endProject")
                            )
                            );
            }
            rs.close();
        }catch(SQLException e){
            System.err.println(e);
        }catch(Exception e){
            System.err.println(e);
        }finally{
        //finally block used to close resources
        try{
            if(stm != null)
                conn.close();
        }catch(Exception e){
            System.err.println("Nu s-a putut inchide conexiunea "+e);
        }
        }    
        return this.projects;
    }

    @Override
    public List<Project> getProject(String id) {
        projects = new ArrayList<>();
        try {
            stm = dbConnect.getStatement();
            sql = "SELECT * FROM Project WHERE isDelete =0 AND idProject ='"+
                    id+"'";
            rs = stm.executeQuery(sql);
            while(rs.next()){
                projects.add(
                        new Project(
                                rs.getString("idProject"),
                                rs.getString("idEmployee"),
                                rs.getString("projectName"),
                                rs.getString("description"),
                                rs.getDate("startProject"),
                                rs.getDate("endProject")
                            )
                            );
            }
            rs.close();
        }catch(SQLException e){
            System.err.println(e);
        }catch(Exception e){
            System.err.println(e);
        }finally{
        //finally block used to close resources
        try{
            if(stm != null)
                conn.close();
        }catch(Exception e){
            System.err.println("Nu s-a putut inchide conexiunea "+e);
        }
        }    
    return projects;
    }

    @Override
    public void updateProject(Project project) {
        try {
                    stm = dbConnect.getStatement();
                    sql = "UPDATE Project SET "
                           + "projectName = '" + project.getProjectName()
                           +"', description = '"+project.getDescription()
                           +"', startProject = (STR_TO_DATE('"+dc.dateToString(project.getStartProject())+"','%Y-%m-%d'))"
                            +", endProject = (STR_TO_DATE('"+dc.dateToString(project.getEndProject())+"','%Y-%m-%d'))"
                           +" WHERE idProject = '"+project.getIdProject()
                           +"' AND idEmployee ='"+project.getIdEmployee()+"'";
                    stm.executeUpdate(sql);
                } catch (SQLException e) {
                    System.err.println("\n---> ProjectDAOImpl "+e);
                }finally{
                 try{
                    if(stm!=null)
                        conn.close();
                }catch(SQLException e){
                    System.err.println(e);
                }
                }
    }

    @Override
    public void deleteProject(Project project) {
            try {
                    stm = dbConnect.getStatement();
                    sql = "UPDATE Project SET isDelete = '1'"
                          +" WHERE idProject = '"+project.getIdProject()+"'";
                    stm.executeUpdate(sql);
                } catch (SQLException e) {
                    System.err.println("\n---> ProjectDAOImpl "+e);
                }finally{
                 try{
                    if(stm!=null)
                        conn.close();
                }catch(SQLException e){
                    System.err.println(e);
                }
                }
    }
    
    @Override
    public void finishProject(Project project){
       try {
                    stm = dbConnect.getStatement();
                    sql = "UPDATE Project SET endProject = (STR_TO_DATE('"+dc.dateToString(new Date())+"','%Y-%m-%d'))"
                          +" WHERE idProject = '"+project.getIdProject()+"'";
                    stm.executeUpdate(sql);
                } catch (SQLException e) {
                    System.err.println("\n---> ProjectDAOImpl "+e);
                }finally{
                 try{
                    if(stm!=null)
                        conn.close();
                }catch(SQLException e){
                    System.err.println(e);
                }
                }
    }
    
}
