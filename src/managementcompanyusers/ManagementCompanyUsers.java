package managementcompanyusers;

import Controler.DateConvertor;
import DataAccessObject.FixedSalaryEmployeeDAO;
import DataAccessObject.FixedSalaryEmployeeDAOImpl;
import DataAccessObject.ProjectDAO;
import DataAccessObject.ProjectDAOImpl;
import Model.FixedSalaryEmployee;
import Model.Project;
import Model.SalesConsultant;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ManagementCompanyUsers {
    private static ProjectDAO empManager;
    private static FixedSalaryEmployeeDAO FSEmp;
    private static DateConvertor dc;
    private static Project project1 ;
    private static Project project2 ;
    private static Project project3 ;
    private static Project project4 ;
    private static FixedSalaryEmployee emp1;
    private static List<SalesConsultant> emps;
    private static Set<Project> projects = new LinkedHashSet<>();
    public static void main(String[] args){
        dc = new DateConvertor();
        empManager = new ProjectDAOImpl();
        FSEmp = new FixedSalaryEmployeeDAOImpl();
        emp1=(new FixedSalaryEmployee("1","Radu","male","D2","senior",new Date(),dc.stringToDate("25-01-2020"),28000,12));
        //FSEmp.updateFSEmployee(emp1);
        
        //System.out.println(dc.dateToString(new Date()));
        
        
        /*
        FSEmp.addFSEmployee(new FixedSalaryEmployee("1","Marius","male","D1","junior",new Date(),dc.stringToDate("25-01-2020"),2800,2));
        FSEmp.addFSEmployee(new FixedSalaryEmployee("2","Loredana","female","D1","middle",new Date(),dc.stringToDate("25-01-2019"),3600,3));
        FSEmp.addFSEmployee(new FixedSalaryEmployee("3","Ionut","male","D1","senior",new Date(),dc.stringToDate("25-01-2023"),8200,5));
        FSEmp.addFSEmployee(new FixedSalaryEmployee("4","Andreea","female","D1","junior",new Date(),dc.stringToDate("25-01-2030"),1800,1));
        FSEmp.addFSEmployee(new FixedSalaryEmployee("5","Stefan","male","D1","middle",new Date(),dc.stringToDate("25-01-2018"),3750,4));
        */
        /*
        dc = new DateConvertor();
        project1 = new Project("1","1","Project1","descriere1",null,null,null);
        project2 = new Project("1","2","Project2","descriere2",null,null,null);
        project3 = new Project("1","3","Project1","descriere3",null,null,null);
        project4 = new Project("1","4","Project1","descriere4",null,null,null);
        */
        
        
        //empManager.finishProject("1");
        //System.out.println(dc.dateToString(new Date()));
        /*
        empManager.addProject(project1);
        empManager.addProject(project2);
        empManager.addProject(project3);
        empManager.addProject(project4);*/
        /*
        empManager.addSalesConsultant(emp1);
        empManager.addSalesConsultant(emp2);
        empManager.addSalesConsultant(emp3);
        empManager.addSalesConsultant(emp4);
        */
        
        
    }
}
